<!--
     This form lets you request hosting of a new or existing project on
     freedesktop.org.

     freedesktop.org provides hosting to projects who provide infrastructure
     to support open-source desktops and user interfaces. We do not provide
     hosting to complete desktops themselves: for instance, we would not host
     a new window manager or desktop environment, but we do host underlying
     graphics, input, and window system technology. We host the GStreamer and
     PulseAudio multimedia frameworks, but we would not host a new video
     player.

     We provide hosting on gitlab.freedesktop.org for projects, including Git
     repositories, issue tracking, merge requests and code review, and GitLab
     CI to run build/etc pipelines. GitLab Pages is also available, allowing
     you to serve and control yourprojectname.freedesktop.org, or if you prefer,
     a custom domain name (e.g. your projectname.org).

     Outside of GitLab, we also offer mailing list hosting on
     lists.freedesktop.org through Mailman. We can also host this on a custom
     domain if you would like.

     We would like to get a complete picture of what your project currently uses
     from us, as well as anything you would want us to do for you as well.
     Please take the below as a guide and fill in all relevant information it
     requests, but if you would like anything else, if you have any questions
     about any of the services, or just want to note something, please include
     it as freeform text.

     As you fill the sections, please check the boxes at the top; please also
     pay close attention to the contact and CoC sections and check those as
     appropriate.

     Thanks!
-->

## Checklist

- [ ] Project details filled by requestor
- [ ] Project contacts filled by by requestor
- [ ] CoC section filled by requestor
- [ ] GitLab project information filled by requestor (or N/A)
- [ ] Mailman information filled by requestor (or N/A)
- [ ] Web hosting information filled by requestor (or N/A)
- [ ] Project creation request acked & scheduled by admin
- [ ] Project created

## Project details
<!--
     Please describe in detail what your project is, what it does, and how it
     aligns with freedesktop.org's aims and mission described above.
-->



## Project contacts
<!--
     This is required for all projects; we will use it this if we need a
     decision representing the project as a whole. For instance, if someone is
     requesting access to your project, if copyright/license issues arise, or if
     we need to discuss something which affects all our member projects (e.g.
     the move to GitLab, or the introduction of the Code of Conduct), we will
     get in touch with those listed here and assume you are able to speak on
     behalf of the project.

     We will also make the primary admin (and additional contacts, if you
     request) the owner of the GitLab group. With this, you will be able to
     completely control access to your repos, and to create new repos, without
     fd.o admin intervention.

     Please list multiple contacts! If you need to amend the contacts, just
     reopen this issue and we'll be happy to help. If you prefer to be contacted
     on an email other than what's in your GitLab profile, please list that as
     well.
-->

**Primary project admin** (GitLab username):

**Additional project contacts** (GitLab usernames):


## Code of Conduct
<!--
     This is required for all projects.

     freedesktop.org maintains a unified Code of Conduct, which can be found at
     https://www.freedesktop.org/wiki/CodeOfConduct

     Our CoC extends across all our platforms - GitLab itself, mailing lists,
     hosted sites, and so on. In short, the CoC requires everyone participating
     in discussion on freedesktop.org to engage in good faith, to treat those
     around them like human beings, and to refrain from bullying behaviour,
     harrassment or discrimination.

     We prefer this to be the standard at project level: that contributors can
     walk into any project and expect that they will be treated with respect.
     Therefore, we recommend projects place a link to the CoC in places like
     README or CONTRIBUTING files (as well as a small mention alongside, e.g.,
     links to mailing lists or GitLab). This should contain a list of
     contacts for your project, who contributors who can raise any issues with.

     Whilst fd.o provides a last-level point of escalation for CoC issues,
     such as when people are uncomfortable raising issues with the project
     team directly, we prefer this to be a part of every project's day-to-day
     communications and ethos, rather than enforced on you from above.
-->

- [ ] My project agrees to the Code of Conduct
- [ ] Where reasonable, we have placed links to the CoC within our project

**CoC contacts** (if different from overall project contacts):


## GitLab project
<!--
     Please delete this section if you do not require Git hosting. Else, select
     a name for your new GitLab group and we will create it for you, adding you
     as admins.
-->

- [ ] New GitLab group name:

## Mailman lists
<!--
     If you would like to have us host a mailing list for you on
     lists.freedesktop.org, please enter details here. We can also host your
     mail on a custom domain if you would like.

     If the admins/moderators for the list should be different to the overall
     project admins, please also list those here.
-->

- [ ] Mailman list: foo@lists.freedesktop.org

## Web hosting
<!--
     We also offer web hosting through GitLab Pages, which uses a GitLab CI
     pipeline to generate and upload content. This will be available at
     projectname.freedesktop.org, or again we can host on a custom domain
     on request.
-->

- [ ] Pages domain: foo.freedesktop.org for repo https://gitlab.freedesktop.org/bar/baz

/label ~"New projects"